
#Algo reconnassance Faciale avec OpenCV et Face_recognition
#Pajak Alexandre 2021
import face_recognition
import cv2
import numpy as np

# récupération du flux vidéo webcam
video_capture = cv2.VideoCapture(0)

# chargement de la base de donnée depuis fichier image
obama_image = face_recognition.load_image_file("images/obama.jpg")
bradpitt_image = face_recognition.load_image_file("images/bradpitt.jpg")
will_image = face_recognition.load_image_file("images/will.jpg")
moi_image = face_recognition.load_image_file("images/moi.jpg")
scarlette_image = face_recognition.load_image_file("images/scarlette.jpg")
malia_image = face_recognition.load_image_file("images/Malia.JPG")


obama_face_encoding = face_recognition.face_encodings(obama_image)[0]
bradpitt_face_encoding = face_recognition.face_encodings(bradpitt_image)[0]
will_face_encoding = face_recognition.face_encodings(will_image)[0]
moi_face_encoding = face_recognition.face_encodings(moi_image)[0]
scarlette_face_encoding = face_recognition.face_encodings(scarlette_image)[0]
malia_face_encoding = face_recognition.face_encodings(malia_image)[0]

# correspondance visage connu&nom
known_face_encodings = [
    obama_face_encoding,
    bradpitt_face_encoding,
    will_face_encoding,
    moi_face_encoding,
    scarlette_face_encoding,
    malia_face_encoding

]
known_face_names = [
    "Obama",
    "bradpitt",
    "will",
    "moi",
    "scarlette",
    "Malia"
]

# variables pour detection
face_locations = []
face_encodings = []
face_names = []
process_this_frame = True

while True:
    # capture image
    ret, frame = video_capture.read()

    # downScale
    small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

    # convertion BGR (OpenCV) a RGB (face_recognition)
    rgb_small_frame = small_frame[:, :, ::-1]

    if process_this_frame:
        # trouvé les visages
        face_locations = face_recognition.face_locations(rgb_small_frame)
        face_encodings = face_recognition.face_encodings(
            rgb_small_frame, face_locations)

        face_names = []
        for face_encoding in face_encodings:
            # comparaison avec visage connu
            matches = face_recognition.compare_faces(
                known_face_encodings, face_encoding)
            name = "Unknown"
            face_distances = face_recognition.face_distance(
                known_face_encodings, face_encoding)
            best_match_index = np.argmin(face_distances)
            if matches[best_match_index]:
                name = known_face_names[best_match_index]

            face_names.append(name)

    process_this_frame = not process_this_frame

    # affichage
    if len(face_names) == 0 : 
        print("No face detected")   
    else :
        for name in face_names:
            if name == "Unknown":
                print("Unknown face detected")
            else :
                print("Yo "+name+" !")
    for (top, right, bottom, left), name in zip(face_locations, face_names):
        # retour format initial
        top *= 4
        right *= 4
        bottom *= 4
        left *= 4

        # carré rouge
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

        # nom
        cv2.rectangle(frame, (left, bottom - 35),
                      (right, bottom), (0, 0, 255), cv2.FILLED)
        font = cv2.FONT_HERSHEY_DUPLEX
        cv2.putText(frame, name, (left + 6, bottom - 6),
                    font, 1.0, (255, 255, 255), 1)


    cv2.imshow('Video', frame)

    # exit
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# stop webcam
video_capture.release()
cv2.destroyAllWindows()
