#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 2022/12/14
# version ='1.0'
# ---------------------------------------------------------------------------
""" Python Body detection and orientation for Unity's 3D model"""
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
import cv2
import mediapipe as mp
import cvzone
from cvzone.FaceMeshModule import FaceMeshDetector


mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_pose = mp.solutions.pose

# define part of body to detect
left_shoulder = 11
right_shoulder = 12
left_hips = 23
right_hips = 24

# tolerence for distance between part
tolerence = 0.02

# define variable for orientation
static = True
backward = False
forward = False
rotation = False

old_left_shoulder_right_shoulder = 0
old_left_shoulder_left_hips = 0
old_right_shoulder_right_hips = 0
old_left_hips_right_hips = 0


# For webcam input:
cap = cv2.VideoCapture(0)
with mp_pose.Pose(min_detection_confidence=0.5, min_tracking_confidence=0.5) as pose:
    while cap.isOpened():
        success, image = cap.read()
        if not success:
            print("Ignoring empty camera frame.")
            # If loading a video, use 'break' instead of 'continue'.
            continue

        # To improve performance, optionally mark the image as not writeable to
        # pass by reference.
        image.flags.writeable = False
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        results = pose.process(image)
        detector = FaceMeshDetector(maxFaces=1)

        if results.pose_landmarks:
            # get coords of part:
            left_shoulder_coords = (
                results.pose_landmarks.landmark[left_shoulder].x,
                results.pose_landmarks.landmark[left_shoulder].y,
            )
            right_shoulder_coords = (
                results.pose_landmarks.landmark[right_shoulder].x,
                results.pose_landmarks.landmark[right_shoulder].y,
            )
            left_hips_coords = (
                results.pose_landmarks.landmark[left_hips].x,
                results.pose_landmarks.landmark[left_hips].y,
            )
            right_hips_coords = (
                results.pose_landmarks.landmark[right_hips].x,
                results.pose_landmarks.landmark[right_hips].y,
            )

            # calculate distance between part
            dist_leftShoulder_rightShoulder = detector.findDistance(
                left_shoulder_coords, right_shoulder_coords
            )[0]
            dist_leftShoulder_leftHips = detector.findDistance(
                left_shoulder_coords, left_hips_coords
            )[0]
            dist_rightShoulder_rightHips = detector.findDistance(
                right_shoulder_coords, right_hips_coords
            )[0]
            dist_leftHips_rightHips = detector.findDistance(
                left_hips_coords, right_hips_coords
            )[0]

            # define min and max for each distance
            min_InterShoulder = old_left_shoulder_right_shoulder * (1 - tolerence)
            max_InterShoulder = old_left_shoulder_right_shoulder * (1 + tolerence)

            min_InterHips = old_left_hips_right_hips * (1 - tolerence)
            max_InterHips = old_left_hips_right_hips * (1 + tolerence)

            min_LeftShoulder_LeftHips = old_left_shoulder_left_hips * (1 - tolerence)
            max_LeftShoulder_LeftHips = old_left_shoulder_left_hips * (1 + tolerence)

            min_RightShoulder_RightHips = old_right_shoulder_right_hips * (
                1 - tolerence
            )
            max_RightShoulder_RightHips = old_right_shoulder_right_hips * (
                1 + tolerence
            )

            # check if distance is reduce or increase
            if (
                dist_leftShoulder_leftHips < min_LeftShoulder_LeftHips
                and dist_rightShoulder_rightHips < min_RightShoulder_RightHips
            ):
                backward = True
                static = False
                forward = False
                # print("backward")
            elif (
                dist_leftShoulder_leftHips > max_LeftShoulder_LeftHips
                and dist_rightShoulder_rightHips > max_RightShoulder_RightHips
            ):
                forward = True
                static = False
                backward = False
                # print("forward")
            # else is static
            else:
                static = True
                backward = False
                forward = False
                # print("static")

            # check rotation if is static
            # if no current rotation save the distance between shoulder as base distance
            if (
                static
                and dist_leftShoulder_rightShoulder < min_InterShoulder
                and dist_leftHips_rightHips < min_InterHips
                and rotation == False
            ):
                base_Dist_Rotation = dist_leftShoulder_rightShoulder
                rotation = True
            # if distance between shoulder is upper or equal to base distance, stop rotation
            if rotation and dist_leftShoulder_rightShoulder > base_Dist_Rotation:
                rotation = False
                print("rotation stop")
            # calculate rotation degree and direction by distance between shoulder/ base distance
            if rotation:
                percent = (dist_leftShoulder_rightShoulder / base_Dist_Rotation) * 100
                # convert to degree
                degree = 90 - (90 * (percent / 100))
                # check direction of rotation by y coordinate of shoulder
                right_shoulder_y = right_shoulder_coords[1]
                left_shoulder_y = left_shoulder_coords[1]
                # if right shoulder is upper than left shoulder, rotation to the right
                if right_shoulder_y > left_shoulder_y:
                    print(f"rotation to the right,degree: {degree}")
                # if left shoulder is upper than right shoulder, rotation to the left
                elif left_shoulder_y > right_shoulder_y:
                    print(f"rotation to the left, degree: {degree}")

            # actualise precedent distances
            old_left_shoulder_right_shoulder = dist_leftShoulder_rightShoulder
            old_left_shoulder_left_hips = dist_leftShoulder_leftHips
            old_right_shoulder_right_hips = dist_rightShoulder_rightHips
            old_left_hips_right_hips = dist_leftHips_rightHips

        # Draw the pose annotation on the image.
        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        mp_drawing.draw_landmarks(
            image,
            results.pose_landmarks,
            mp_pose.POSE_CONNECTIONS,
            landmark_drawing_spec=mp_drawing_styles.get_default_pose_landmarks_style(),
        )
        # Flip the image horizontally for a selfie-view display.
        cv2.imshow("MediaPipe Pose", cv2.flip(image, 1))
        if cv2.waitKey(5) & 0xFF == 27:
            break
cap.release()
